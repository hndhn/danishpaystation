package paystation.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestDanish2Rate {

	PayStation ps;
	  /** Fixture for pay station testing. */
	  @Before
	  public void setUp() {
	    ps = new PayStationImpl( new Danish2RateStrategy(), new DanishCoin() );
	  }

	  /** Test acceptance of all legal coins */
	  @Test
	  public void shouldAcceptLegalCoins() throws IllegalCoinException {
	    ps.addPayment(1);
	    ps.addPayment(2);
	    ps.addPayment(5);
	    assertEquals( "Should accept 1, 2, 5 and 20 kroner", 
	                  48, ps.readDisplay() ); 
	  }
	  
	  @Test 
	  public void shouldDisplay60For10Kroners() throws IllegalCoinException {
		  ps.addPayment(10);
		  assertEquals( "Should display 60 for 10 kroners", 
                  60, ps.readDisplay() ); 
	  }

	  @Test 
	  public void shouldDisplay134For23Kroners() throws IllegalCoinException {
		  ps.addPayment(20);
		  ps.addPayment(20);
		  assertEquals( "Should display 135 for 23 kroners", 
                  220, ps.readDisplay() ); 
	  }

}

