package paystation.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestUSRate {

	PayStation ps;
	  /** Fixture for pay station testing. */
	  @Before
	  public void setUp() {
	    ps = new PayStationImpl( new One2OneRateStrategy(), new USCoin() );
	  }

	  /** Test acceptance of all legal coins */
	  @Test
	  public void shouldAcceptLegalCoins() throws IllegalCoinException {
	    ps.addPayment( 5 );
	    ps.addPayment( 10 );
	    ps.addPayment( 25 );
	    assertEquals( "Should accept 5, 10, and 25 cents", 
	                  5+10+25, ps.readDisplay() ); 
	  }
	  
	 /** Test a single hour parking */
	  @Test public void shouldDisplay120MinFor300cent() {
	    RateStrategy rs = new LinearRateStrategy();
	    assertEquals( 300 / 5 * 2, rs.calculateTime(300) ); 
	  }
	  
}


