package paystation.domain;
// The rate for second Danish county order
public class Danish2RateStrategy implements RateStrategy {
	public int calculateTime( int amount ) {
	    int time = 0;
	    if ( amount >= 10*2) { // from 3rd hour onwards
	      amount -= 20;
	      time = 120 /*min*/ + amount * 5;
	    } else { // up to 1st hour
	      time = amount * 6;
	    }
	    return time;
	  }
}

