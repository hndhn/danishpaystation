package paystation.domain;

public class DanishCoin implements CoinValidationType {
	public void validateCoin(int amount)
			throws IllegalCoinException {
		switch (amount ) {
	    case 1: break;
	    case 2: break;
	    case 5: break;
	    case 10: break;
	    case 20: break;  
	    default: 
	      throw new IllegalCoinException("Invalid coin: "+ amount);
	    }
	}
}

