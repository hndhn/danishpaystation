package paystation.domain;

public class USCoin implements CoinValidationType {
	public void validateCoin(int amount)
			throws IllegalCoinException {
		switch (amount ) {
	    case 5: break;
	    case 10: break;
	    case 25: break;  
	    default: 
	      throw new IllegalCoinException("Invalid coin: "+ amount);
	    }
	}
}

