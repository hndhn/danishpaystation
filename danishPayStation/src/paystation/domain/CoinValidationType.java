package paystation.domain;

public interface CoinValidationType {
	public void validateCoin(int amount) throws IllegalCoinException ;
}

